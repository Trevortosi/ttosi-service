module bitbucket.org/Trevortosi/ttosi-service

go 1.13

require (
	bitbucket.org/newyuinc/adc-services v0.0.0-20191029233744-0fb49ba8e14e
	bitbucket.org/newyuinc/ginto v0.0.0-20190826214336-bc4b892af2d9 // indirect
	bitbucket.org/newyuinc/platform-bus v0.0.0-20190930151818-4f42a0ff9c77
	bitbucket.org/newyuinc/platform-echo v0.0.0-20190903190959-eff8fa26e0e5 // indirect
	bitbucket.org/newyuinc/platform-insights v0.0.0-20190903191100-1d214084e9fc
	bitbucket.org/newyuinc/platform-secrets v0.0.0-20190826220004-b8b07ee72b65 // indirect
	github.com/alexcesaro/statsd v2.0.0+incompatible // indirect
	github.com/aws/aws-sdk-go v1.25.43 // indirect
	github.com/coreos/bbolt v1.3.3 // indirect
	github.com/coreos/etcd v3.3.18+incompatible // indirect
	github.com/coreos/go-semver v0.3.0 // indirect
	github.com/coreos/go-systemd v0.0.0-00010101000000-000000000000 // indirect
	github.com/coreos/pkg v0.0.0-20180928190104-399ea9e2e55f // indirect
	github.com/dgraph-io/badger v1.6.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/dgryski/go-lttb v0.0.0-20180810165845-318fcdf10a77 // indirect
	github.com/evanphx/json-patch v4.5.0+incompatible // indirect
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/golang/groupcache v0.0.0-20191027212112-611e8accdfc9 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/btree v1.0.0 // indirect
	github.com/google/uuid v1.1.1 // indirect
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.1.0 // indirect
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0 // indirect
	github.com/grpc-ecosystem/grpc-gateway v1.12.1 // indirect
	github.com/jonboulle/clockwork v0.1.0 // indirect
	github.com/json-iterator/go v1.1.8 // indirect
	github.com/labstack/echo v3.3.10+incompatible // indirect
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/lithammer/go-jump-consistent-hash v1.0.1 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/moshenahmias/failure v0.0.0-20180520123008-c31a9a4c0318 // indirect
	github.com/nsqio/go-nsq v1.0.7 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible // indirect
	github.com/prometheus/client_golang v1.2.1 // indirect
	github.com/robfig/cron v1.2.0 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/soheilhy/cmux v0.1.4 // indirect
	github.com/tmc/grpc-websocket-proxy v0.0.0-20190109142713-0ad062ec5ee5 // indirect
	github.com/valyala/fasttemplate v1.1.0 // indirect
	github.com/xiang90/probing v0.0.0-20190116061207-43a291ad63a2 // indirect
	go.etcd.io/bbolt v1.3.3 // indirect
	go.etcd.io/etcd v3.3.18+incompatible // indirect
	go.uber.org/zap v1.13.0 // indirect
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	google.golang.org/grpc v1.25.1 // indirect
	gopkg.in/alexcesaro/statsd.v2 v2.0.0 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
	gopkg.in/urfave/cli.v1 v1.20.0
	sigs.k8s.io/yaml v1.1.0 // indirect
)

replace github.com/coreos/go-systemd => github.com/coreos/go-systemd/v22 v22.0.0
