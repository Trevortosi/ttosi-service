#!/bin/bash

exec etcd --data-dir=/etc/service/etcd \
    --name=${NAME} \
    --advertise-client-urls=${ETCD_ADVERTISE_CLIENT_URLS} \
    --listen-client-urls=${ETCD_LISTEN_CLIENT_URLS} \
    --initial-advertise-peer-urls=${ETCD_INITIAL_ADVERTISE_PEER_URLS} \
    --listen-peer-urls=${ETCD_LISTEN_PEER_URLS} \
    --initial-cluster=${CLUSTER} \
    --initial-cluster-state=${CLUSTER_STATE} \
    --initial-cluster-token=${TOKEN}