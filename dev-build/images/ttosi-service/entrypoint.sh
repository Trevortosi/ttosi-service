#!/bin/bash


ENDPOINTS=tt-etcd-0:2379

ETCDCTL_API=3 etcdctl --endpoints=$ENDPOINTS put service/version/ttosi-service 1.0.0
ETCDCTL_API=3 etcdctl --endpoints=$ENDPOINTS put service/config/ttosi-service/1.0.0 </src/config/dev.json

src/dev-build/output/ttosi-service --etcd-client-endpoints=tt-etcd-0:2379 1.0.0 ttosi-service