package main

import (
	"os"

	"bitbucket.org/newyuinc/platform-insights/pkg/service"
)

func main() {
	svcName := os.Args[len(os.Args)-1]
	svcVersion := os.Args[len(os.Args)-2]

	svc := &DummyService{}
	host := service.NewHost(svcName, svcVersion, svc)
	_ = host.Start()
}
