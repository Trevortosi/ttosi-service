package main

import (
	"fmt"
	"os"

	cfgetcd "bitbucket.org/newyuinc/adc-services/pkg/cfg/etcd"
	msub "bitbucket.org/newyuinc/platform-bus/pkg/consumer"
	"bitbucket.org/newyuinc/platform-insights/pkg/service"
	"gopkg.in/urfave/cli.v1"
)

type DummyService struct {
	h *service.Host
	// l         *zap.Logger
	c *msub.Consumer
	// p         *mpub.Publisher
	// glv       *ginto.Ginto
	// scheduler *cron.Cron
}

type Configuration struct {
	Consumer            *msub.Configuration         `json:"mbus"`
	GlobalConfiguration cfgetcd.ReaderConfiguration `json:"config"`
}

func (s *DummyService) InitializeConfiguration(app *cli.App, h *service.Host) service.ConfigurationFormatter {
	s.h = h
	return s
}

func (s *DummyService) Start(c *cli.Context, config interface{}) error {
	return s.startServer(config)
}

func (s *DummyService) startServer(config interface{}) error {
	sc := config.(*Configuration)
	fmt.Println("Service has been started!!!")
	fmt.Printf("service config mbus %v\n", *sc.Consumer)
	hn, err := os.Hostname()
	if err != nil {
		hn = "-"
	}

	c, err := msub.New(hn, s.h.ServiceName, sc.Consumer)
	if err != nil {
		return err
	}

	if err = c.Start(); err != nil {
		return err
	}

	s.c = c

	return nil

}

func (s *DummyService) Stop() {
	if s.c != nil {
		s.c.Stop()
	}
}

func (s *DummyService) ConfigurationUpdated(config interface{}) {
	s.Stop()
	err := s.startServer(config)
	if err != nil {
		fmt.Printf("couldNotReStartAfterConfigUpdate: %v", err)
	}
}

func (s *DummyService) StillAlive() error {
	if s.c != nil {
		return nil
	}

	return fmt.Errorf("serverNotStartedOrInError")
}

func (s *DummyService) ProvideInstance() interface{} {
	return &Configuration{}
}
